#! /usr/bin/env python
import setuptools


setuptools.setup(
    name="demo",
    version="0.0.1",
    author="WeatherForce",
    author_email="contact@weatherforce.org",
    description="demo dashboard",
    packages=setuptools.find_packages(exclude=["tests", "tests.*"]),
    install_requires=[
        'numpy',
        'pandas',
        'plotly',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    package_data={'': ['*.yml']}
)
